﻿using System.Collections;
using System.Linq;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace NetGame
{
    public class PlayerController : MonoBehaviourPunCallbacks, IPunObservable
    {
        private Controls _controls;
        private Transform _target;
        private GameManager _manager;
        private Transform _bulletPool;
        private bool _deathStage = false; // Переменная для проверки стадии игры
        
        [SerializeField] private Rigidbody _rb;
        [SerializeField] private TextMeshProUGUI _hpText; // Текстовое поле шкалы здоровья
        [SerializeField] private Slider _hpBar; // Шкала здоровья
        [SerializeField] private ParticleSystem _explosion; // Эффект получения урона
        [SerializeField] private ParticleSystem _death; // Эффект смерти
        [SerializeField] private ProjectileController _bulletPrefab;
        [SerializeField] private Transform _firePoint;
        [SerializeField] private PhotonView _photonView;

        [Header("Settings")]
        [SerializeField, Range(1f, 10f)]
        private float _speed = 2f;
        [SerializeField, Range(.5f, 5f)]
        private float _maxSpeed = 2f;
        [Range(1f, 50f)]
        public float Health = 5f;
        [SerializeField, Range(.1f, 1f)]
        private float _attackDelay = .4f;
        [SerializeField, Range(.1f, 1f)]
        private float _rotationDelay = .2f;
        
        void Start()
        {
            _rb = GetComponent<Rigidbody>();
            _bulletPool = FindObjectOfType<EventSystem>().transform;

            // Выставление параметров шкалы
            _hpText.text = Health.ToString();
            _hpBar.maxValue = Health;
            _hpBar.value = Health;
            _hpBar.minValue = 0f;
            
            _controls = new Controls();

            _manager = FindObjectOfType<NetGame.GameManager>();
            _manager.AddPlayer(this);

            // Фиксация камеры
            if (_photonView.IsMine)
                _manager.AttachCamera(transform);
        }
        
        void FixedUpdate()
        {
            // Поворот шкал к камере
            _hpBar.transform.rotation = Camera.main.transform.rotation;
            
            if (!_photonView.IsMine) return;

            var dir = _controls.Player1.Movement.ReadValue<Vector2>();
            if (dir.x == 0 && dir.y == 0) return;

            var velocity = _rb.velocity;
            velocity += new Vector3(dir.x, 0f, dir.y) * _speed * Time.fixedDeltaTime;
            velocity.y = 0f;
            velocity = Vector3.ClampMagnitude(velocity, _maxSpeed);
            _rb.velocity = velocity;
        }

        public void SetTarget(Transform target)
        {
            _target = target;
            
            if (!_photonView.IsMine) return;
            _controls.Player1.Enable();
            StartCoroutine(Fire());
            StartCoroutine(Focus());
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.name.Contains("Wall")) // Смерть игрока при выходе за пределы поля
            {
                Health = -1f;
            }
            else
            {
                var bullet = other.GetComponent<ProjectileController>();
                if (bullet == null || bullet.Parent == name) return;

                Health -= bullet.GetDamage;
                Destroy(other.gameObject);
            }

            _explosion.Play();

            _hpBar.value = Health > 0 ? Health : 0f;
            _hpText.text = Health > 0 ? Health.ToString() : "0";

            if (Health <= 0 && !_deathStage)
            {
                _deathStage = true;
                _manager.EndOfGame(this); // Запуск завершения игры
            }
        }

        private IEnumerator Fire()
        {
            // Добавила задержку перед началом стрельбы, чтобы игроки могли начать двигаться и занять позиции
            yield return new WaitForSeconds(_attackDelay * 5f);
            
            while (!_deathStage) // При переходе к заверешнию игры стрельба прекращается
            {
                // Синхронизация пуль выполнена при помощи метода, вызываемого на всех клиентах (RPC) 
                object[] args = {_firePoint.position, transform.rotation, name};
                _photonView.RPC("CreateBullet", RpcTarget.AllBuffered, args);
                yield return new WaitForSeconds(_attackDelay);
            }
        }

        [PunRPC] // Метод создания пули
        public void CreateBullet(Vector3 firePos, Quaternion rotation, string parentName)
        {
            var bullet = Instantiate(_bulletPrefab, firePos, rotation, _bulletPool);

            // Покрас пули по игроку (был сделан в процессе дебаггинга, но решила оставить для красоты)
            var creator = GameObject.FindObjectsOfType<PlayerController>().FirstOrDefault(t => t.name == parentName);
            bullet.GetComponent<MeshRenderer>().material = creator.GetComponent<MeshRenderer>().material;
            
            bullet.GetComponent<ProjectileController>().Parent = parentName;
        }

        // Отображения сообщения
        public void ActivateWinStateMessage(bool loser)
        {
            // Эффект смерти запускается на всех клиентах
            if (loser) _death.Play();
            
            if (!_photonView.IsMine) return;
            _controls.Player1.Disable();
            StartCoroutine("BackToMenu", loser);
        }
        
        private IEnumerator Focus()
        {
            while (true)
            {
                transform.LookAt(_target);
                transform.eulerAngles = new Vector3(0f, transform.eulerAngles.y, 0f);
                
                yield return new WaitForSeconds(_rotationDelay);
            }
        }

        // Переход к меню
        private IEnumerator BackToMenu(bool loser)
        {
            yield return new WaitForSeconds(2f); // Пауза для проигрывания эффекта смерти
            _manager.ShowMessage(loser ? "you lose" : "you win", loser);
            yield return new WaitForSeconds(2f);
            PhotonNetwork.LeaveRoom();
        }

        private void OnDestroy()
        {
            _controls.Player1.Disable();
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting) // owner
            {
                stream.SendNext(PlayerData.Create(this));
            }
            else // readers
            {
                ((PlayerData)stream.ReceiveNext()).Set(this);
            }
        }
    }
}
