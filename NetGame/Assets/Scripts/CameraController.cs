﻿using System.Collections;
using UnityEngine;

namespace NetGame
{
    public class CameraController : MonoBehaviour
    {
        [SerializeField] private Vector3 _offset; // Дельта расстояния между камерой и игроком
        private Transform _target; // Игрок, за которым надо следить
        private bool move = false; // Переменная для проверки обновления камеры, нужна для избежания ошибки при возвращении в сцену меню
        
        // Фиксация камеры на игроке
        public void SetCamera(Transform target)
        {
            _target = target;
            move = true;
            StartCoroutine(Movement());
        }

        // Остановка обновления позиции камеры
        public void StopMovement()
        {
            move = false;
        }

        private IEnumerator Movement()
        {
            while (move)
            {
                transform.position = _target.position + _offset;
                yield return null;
            }
        }
    }
}