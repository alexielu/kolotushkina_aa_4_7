﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

namespace NetGame
{
    public static class Debugger
    {
        private static TextMeshProUGUI _console;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        public static void OnStart()
        {
            _console = GameObject.FindObjectsOfType<TextMeshProUGUI>().FirstOrDefault(t => t.name == "Console");
        }
        
        public static void Log(object message)
        {
#if UNITY_EDITOR
            Debug.Log(message);
#elif UNITY_STANDALONE_WIN && !UNITY_EDITOR
            _console.text += message;
#endif
        }

        public static byte[] SerializePlayerData(object data)
        {
            var player = (PlayerData)data;
            var array = new List<byte>(16);
            
            array.AddRange(BitConverter.GetBytes(player.posX));
            array.AddRange(BitConverter.GetBytes(player.posZ));
            array.AddRange(BitConverter.GetBytes(player.rotY));
            array.AddRange(BitConverter.GetBytes(player.health));

            return array.ToArray();
        }

        public static object DeserializePlayerData(byte[] data)
        {
            return new PlayerData
            {
                posX = BitConverter.ToSingle(data,0),
                posZ = BitConverter.ToSingle(data,4),
                rotY = BitConverter.ToSingle(data,8),
                health = BitConverter.ToSingle(data,12)
            };
        }
    }

    public struct PlayerData
    {
        public float posX;
        public float posZ;
        public float rotY;
        public float health;

        public static PlayerData Create(PlayerController player)
        {
            return new PlayerData
            {
                posX = player.transform.position.x,
                posZ = player.transform.position.z,
                rotY = player.transform.eulerAngles.y,
                health = player.Health
            };
        }

        public void Set(PlayerController player)
        {
            var vectorT = player.transform.position;
            var vectorR = player.transform.eulerAngles;
            
            vectorT.x = posX;
            vectorT.z = posZ;
            vectorR.y = rotY;

            player.transform.position = vectorT;
            player.transform.eulerAngles = vectorR;

            player.Health = health;
        }
    }
}
