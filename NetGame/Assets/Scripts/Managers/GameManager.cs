﻿using ExitGames.Client.Photon;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace NetGame
{
    public class GameManager : MonoBehaviourPunCallbacks
    {
        private PlayerController _player1;
        private PlayerController _player2;

        [SerializeField] private CameraController _cam;
        [SerializeField] private Image _panel; // Панель с сообщением о победе / проигрыше
        [SerializeField] private TextMeshProUGUI _endOfGameMessage; // Текстовое поле сообщения
        [SerializeField] private string _playerName;
        [SerializeField, Range(1f, 10f)]
        private float _spawnInterval = 7f;
        
        [SerializeField] private InputAction _quitGame;

        void Start()
        {
            _quitGame.Enable();
            _quitGame.performed += OnQuit;

            var rndPos = new Vector3(Random.Range(-_spawnInterval, _spawnInterval), 0f, Random.Range(-_spawnInterval, _spawnInterval));
            var GO = PhotonNetwork.Instantiate(_playerName + PhotonNetwork.NickName, rndPos, Quaternion.identity);

            PhotonPeer.RegisterType(typeof(PlayerData), 100, Debugger.SerializePlayerData,
                Debugger.DeserializePlayerData);
        }

        public void AddPlayer(PlayerController player)
        {
            if (player.name.Contains("1"))
                _player1 = player;
            else _player2 = player;

            if (_player1 != null && _player2 != null)
            {
                _player1.SetTarget(_player2.transform);
                _player2.SetTarget(_player1.transform);
            }
        }

        // Метод фиксации камеры на активном персонаже
        public void AttachCamera(Transform player)
        {
            _cam.SetCamera(player);
        }

        // Метод вызова сообщений о победе / проигрыше
        public void EndOfGame(PlayerController loser)
        {
            _player1.ActivateWinStateMessage(_player1 == loser);
            _player2.ActivateWinStateMessage(_player2 == loser);
        }
        
        // Отображение сообщения с фоном
        public void ShowMessage(string message, bool loser)
        {
            _panel.gameObject.SetActive(true);
            _panel.color = loser ? Color.black : Color.white;
            _endOfGameMessage.text = message;
        }
        
        public override void OnLeftRoom()
        {
            _cam.StopMovement();
            SceneManager.LoadScene(0);
        }

        private void OnQuit(InputAction.CallbackContext obj)
        {
            PhotonNetwork.LeaveRoom();
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_STANDALONE_WIN && !UNITY_EDITOR
            Application.Quit();
#endif
        }

        private void OnDestroy()
        {
            _quitGame.Dispose();
        }
    }
}