﻿using Photon.Pun;
using UnityEngine;

namespace NetGame
{
    public class MenuManager : MonoBehaviourPunCallbacks
    {
        void Start()
        {
#if UNITY_EDITOR
            PhotonNetwork.NickName = "1";
#elif UNITY_STANDALONE_WIN && !UNITY_EDITOR
        PhotonNetwork.NickName = "2";
#endif

            PhotonNetwork.AutomaticallySyncScene = true;
            PhotonNetwork.GameVersion = "0.0.1";
            PhotonNetwork.ConnectUsingSettings();
        }

        public override void OnConnectedToMaster()
        {
            Debugger.Log("Ready to connect");
        }

        public override void OnJoinedRoom()
        {
            PhotonNetwork.LoadLevel("GameScene");
        }

        public void OnCreateRoom_Editor()
        {
            PhotonNetwork.CreateRoom(null, new Photon.Realtime.RoomOptions {MaxPlayers = 2});
        }

        public void OnJoinRoom_Editor()
        {
            PhotonNetwork.JoinRandomRoom();
        }

        public void OnQuit_Editor()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
            
#elif UNITY_STANDALONE_WIN && !UNITY_EDITOR
            Application.Quit();

#endif
        }
    }
}